import React, { Component } from 'react';


class Innerbox extends Component {

    constructor() {
        super();

        this.state = {
            letter: "",
            winMessage: "",
            ids: [1, 2, 3, 4, 5, 6, 7, 8, 9],
            check: true,
            reset: "Reset",
            play: true,
            win : [
                ['1','2','3'],['4','5','6'],['7','8','9'],['1','4','7'],['2','5','8'],['1','5','9'],['3','5','7'],['3','6','9']
            ],
            checkDraw : 0,
            buttonStyle : {
                marginTop: "10px",
                padding: "10px",
                width: "100px",
                borderRadius: "20px",
                alignSelf: "center",
                color: "white",
                border: "none",
                backgroundColor: "black",
                cursor: "pointer",
            }
        }
        this.xArray = [];
        this.oArray = [];

    }

    letterStyle = {
        fontSize: "90px",
        textAlign: "center",
        color: "white"
    }

    write = (event) => {
        if (this.state.play) {
            if (this.state.check && event.target.innerHTML === "") {

                event.target.innerHTML = "x";
                this.xArray.push(event.target.id);
                let count = this.state.checkDraw + 1;
                this.setState({
                    check: false,
                    checkDraw : count
                })
                if(this.state.checkDraw === 8) {
                    this.setState({
                        play: false,
                        winMessage : "Match Draw",
                        reset: "Restart",
                        checkDraw: 0
                    })
                }
                this.state.win.forEach(indices => {
                    let xWin = indices.every(index => this.xArray.includes(index));
                    if(xWin) {
                        this.setState({
                            play: false,
                            winMessage : "Player 'X' wins",
                            checkDraw : 0
                        })
                    }
                })
                console.log(this.state.checkDraw);


            } else if (event.target.innerHTML === "") {

                event.target.innerHTML = "o";
                this.oArray.push(event.target.id)
                let count = this.state.checkDraw + 1;
                this.setState({
                    check: true,
                    checkDraw : count
                })
                if(this.state.checkDraw === 8) {
                    this.setState({
                        play: false,
                        winMessage : "Match Draw",
                        reset: "Restart",
                        checkDraw: 0
                    })
                }
                this.state.win.forEach(indices => {
                    let oWin = indices.every(index => this.oArray.includes(index));
                    if(oWin) {
                        this.setState({
                            play: false,
                            winMessage : "Player 'O' wins",
                            checkDraw : 0
                        })
                    } 
                })

            }
        }
    }

    resetGame = () => {
        this.setState({
            play : true,
            check : true,
            winMessage : "",
            reset : "Reset"
        })
        this.xArray = [];
        this.oArray = [];
        console.log(this.xArray , this.oArray);
        document.querySelectorAll(".innerBox").forEach(box => {
            box.innerHTML = "";
        })
    }
    btnHover = () => {
        this.setState({
            buttonStyle : {
                marginTop: "10px",
                padding: "10px",
                width: "100px",
                borderRadius: "20px",
                alignSelf: "center",
                color: "black",
    
                backgroundColor: "white",
                cursor: "pointer",
                border: "1px solid black",
            }
        })
    }
    btnChange = () => {
        this.setState({
            buttonStyle : {
                marginTop: "10px",
                padding: "10px",
                width: "100px",
                borderRadius: "20px",
                alignSelf: "center",
                color: "white",
                border: "none",
                backgroundColor: "black",
                cursor: "pointer",
           
            }
        })
    }

    playerWon = {
        display: "flex",
        alignSelf : "center",
        color : "#262626",
        fontSize : "30px",
    }

    // innerBox = this.state.ids.map(key => 
    //     <div className="innerBox" onClick={this.write} key={key} style={this.letterStyle}>{this.state.letter}</div>
    // )
    render() {
        return (
            <React.Fragment>
                <div className="outerBox">
                    {
                        this.state.ids.map(key =>
                            <div className="innerBox" onClick={this.write} id={key} key={key} style={this.letterStyle}>{this.state.letter}</div>
                        )
                    }
                </div>
                <br />
                <div className="playerWon" style={this.playerWon}>{this.state.winMessage}</div>
                <button className="button" onMouseEnter={this.btnHover} onMouseOut={this.btnChange} style={this.state.buttonStyle} onClick={this.resetGame}>{this.state.reset}</button>
            </React.Fragment>
        )
    }
}

export default Innerbox;
